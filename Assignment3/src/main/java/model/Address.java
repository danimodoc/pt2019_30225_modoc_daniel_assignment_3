package model;
/**
 * 
 * @author danimodoc
 * Class to represent the address table
 */

public class Address {

	private int id;
	private int zip;
	private String city;
	private String street;
	private int customerid;

	public Address() {

	}

	public Address(int id, int zip, String city, String street) {
		super();
		this.id = id;
		this.zip = zip;
		this.city = city;
		this.street = street;
	}

	public Address(int zip, String city, String street,int customerid) {
		super();
		this.zip = zip;
		this.city = city;
		this.street = street;
		this.customerid=customerid;
	}

	@Override
	public String toString() {
		return "Address [zip=" + zip + ", city=" + city  + ", street=" + street + "]";
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getCustomerid() {
		return customerid;
	}

	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}
	
	

}

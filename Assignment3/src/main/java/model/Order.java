package model;

/**
 * 
 * @author danimodoc
 * Class to represent the order table
 */

public class Order {
	
	private int id;
	private int customerid;
	private int productid;
	
	public Order(int id, int productid, int customerid) {
		this.id = id;
		this.customerid = customerid;
		this.productid = productid;
	}
	
	public Order( int productid, int customerid) {
		this.customerid = customerid;
		this.productid = productid;
	}
	
	public Order() {
		
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public int getCustomerid() {
		return customerid;
	}

	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

}

package bll.validators;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dataAccessLayer.ProductDa;
import dataAccessLayer.ProductDa;
import model.Product;

public class ProductBLL {
	
	private List<Validator<Product>> validators;

	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		//validators.add(new EmailValidator());
		//validators.add(new AgeValidator());
	}

	public Product findProductById(int id) {
		Product st = ProductDa.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertProduct(Product Product) {
		for (Validator<Product> v : validators) {
			v.validate(Product);
		}
		return ProductDa.insert(Product);
	}
}

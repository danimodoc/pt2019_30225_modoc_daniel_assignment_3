package bll.validators;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import dataAccessLayer.CustomerDa;

import model.Customer;

public class CustomerBLL {
	
	private List<Validator<Customer>> validators;

	public CustomerBLL() {
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new AgeValidator());
	}

	public Customer findCustomerById(int id) {
		Customer st = CustomerDa.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Customer with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertCustomer(Customer Customer) {
		for (Validator<Customer> v : validators) {
			v.validate(Customer);
		}
		return CustomerDa.insert(Customer);
	}

}

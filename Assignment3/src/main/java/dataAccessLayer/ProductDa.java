package dataAccessLayer;

import java.sql.Connection;

/**
 *  Class contains the methods that operate on the table from the database
 * 
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Customer;
import model.Product;

public class ProductDa {
	
	 protected static final Logger LOGGER = Logger.getLogger(CustomerDa.class.getName());
	 private static final String insertStatementString = "INSERT INTO product (name,price,quantity)" + "VALUES(?,?,?)";
	 private static final String findStatementString = "SELECT * FROM product where id=?";
	 private static final String editStatementString = "update product set name=?,price=?,quantity=? where id=?";
	 private static final String deleteStatementString = "delete FROM product where id=?";
	 
     public static Product findById(int productId) {
		 
		 Product toReturn = null;
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(findStatementString);
			 findStatement.setLong(1, productId);
			 rs=findStatement.executeQuery();
			 rs.next();
			 
			 String name = rs.getString("name");
			 int price = rs.getInt("price");
			 int quantity = rs.getInt("quantity");
			 toReturn = new Product(productId,name,price,quantity);
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "ProductDa:findById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 return toReturn;
	 }
     
     public static int insert(Product product)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement insertStatement = null;
		 int insertedId= -1;
		 try 
		 {
			 insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			 insertStatement.setString(1,product.getName());
			 insertStatement.setInt(2, product.getPrice());
			 insertStatement.setInt(3, product.getQuantity());
			 insertStatement.executeUpdate();
			 
			 ResultSet rs = insertStatement.getGeneratedKeys();
			 
			 if(rs.next())
			 {
				 insertedId=rs.getInt(1);
			 }
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDA:insert "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(insertStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 
		 return insertedId;
	 }
     
     public static Product update(int productId,Product product)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement editStatement = null;
		 Product toReturn = null;
		 
		 try 
		 {
			 editStatement = dbConnection.prepareStatement(editStatementString);
			 editStatement.setLong(4, productId);
			 editStatement.setString(1, product.getName());
			 editStatement.setString(2, String.valueOf(product.getPrice()));
			 editStatement.setString(3, String.valueOf(product.getQuantity()));
			 editStatement.executeUpdate();
			 
			 toReturn = new Product(productId,product.getName(),product.getPrice(),product.getQuantity());
			 
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDA:updated "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(editStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 
		 return toReturn;
	 }
     
     public static void deleteById(int productId) {
		 
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(deleteStatementString);
			 findStatement.setLong(1, productId);
			 findStatement.executeUpdate();
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "ProductDa:deleteById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
	 }

}

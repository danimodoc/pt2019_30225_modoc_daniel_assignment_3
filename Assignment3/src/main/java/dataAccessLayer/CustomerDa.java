package dataAccessLayer;

import java.sql.Connection;

/**
 *  Class contains the methods that operate on the table from the database
 * 
 */

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import connection.ConnectionFactory;
import model.Customer;

public class CustomerDa {
    
	 protected static final Logger LOGGER = Logger.getLogger(CustomerDa.class.getName());
	 private static final String insertStatementString = "INSERT INTO customer (name,address,email,age)" + "VALUES(?,?,?,?)";
	 private static final String findStatementString = "SELECT * FROM customer where id=?";
	 private static final String deleteStatementString = "delete FROM customer where id=?";
	 private static final String editStatementString = "update customer set name=?,address=?,email=?,age=? where id=?";
	 
	 
	 public static Customer findById(int customerId) {
		 
		 Customer toReturn = null;
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(findStatementString);
			 findStatement.setLong(1, customerId);
			 rs=findStatement.executeQuery();
			 rs.next();
			 
			 String name = rs.getString("name");
			 String address = rs.getString("address");
			 String email = rs.getString("email");
			 int age = rs.getInt("age");
			 toReturn = new Customer(customerId,name,address,email,age);
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDa:findById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 return toReturn;
	 }
	 
	 public static int insert(Customer customer)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement insertStatement = null;
		 int insertedId= -1;
		 try 
		 {
			 insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			 insertStatement.setString(1, customer.getName());
			 insertStatement.setString(2, customer.getAddress());
			 insertStatement.setString(3, customer.getEmail());
			 insertStatement.setInt(4, customer.getAge());
			 insertStatement.executeUpdate();
			 
			 ResultSet rs = insertStatement.getGeneratedKeys();
			 
			 if(rs.next())
			 {
				 insertedId=rs.getInt(1);
			 }
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDA:insert "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(insertStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 
		 return insertedId;
	 }
	 
	 public static Customer update(int customerId,Customer customer)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement editStatement = null;
		 Customer toReturn = null;
		 
		 try 
		 {
			 editStatement = dbConnection.prepareStatement(editStatementString);
			 editStatement.setLong(5, customerId);
			 editStatement.setString(1, customer.getName());
			 editStatement.setString(2, customer.getAddress());
			 editStatement.setString(3, customer.getEmail());
			 editStatement.setInt(4, customer.getAge());
			 editStatement.executeUpdate();
			 
			 toReturn = new Customer(customerId,customer.getName(),customer.getAddress(),customer.getEmail(),customer.getAge());
			 
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDA:updated "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(editStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 
		 return toReturn;
	 }
	 
     public static void deleteById(int customerId) {
		 
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(deleteStatementString);
			 findStatement.setLong(1, customerId);
			 findStatement.executeUpdate();
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDa:deleteById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
	 }
	 
	 public static DefaultTableModel buildTableModel(ResultSet rs)
		        throws SQLException {

		    ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

		    // names of columns
		    Vector<String> columnNames = new Vector<String>();
		    int columnCount = metaData.getColumnCount();
		    for (int column = 1; column <= columnCount; column++) {
		        columnNames.add(metaData.getColumnName(column));
		    }

		    // data of the table
		    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		    while (rs.next()) {
		        Vector<Object> vector = new Vector<Object>();
		        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
		            vector.add(rs.getObject(columnIndex));
		        }
		        data.add(vector);
		    }

		    return new DefaultTableModel(data, columnNames);

		}
	 
	
}

package dataAccessLayer;

import java.sql.Connection;

/**
 *  Class contains the methods that operate on the table from the database
 * 
 */
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.Customer;
import model.Order;
import model.Product;

public class OrderDA {
	
	 protected static final Logger LOGGER = Logger.getLogger(CustomerDa.class.getName());
	 private static final String insertStatementString = "INSERT INTO orders (productid,customerid)" + "VALUES(?,?)";
	 private static final String findStatementString = "SELECT * FROM orders where id=?";
	 private static final String verifyStockStatementString = "SELECT * FROM product where id=?";
	 private static final String clientStatementString = "SELECT * FROM customer where id=?";
	
     public static Order findById(int orderId) {
		 
		 Order toReturn = null;
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(findStatementString);
			 findStatement.setLong(1, orderId);
			 rs=findStatement.executeQuery();
			 rs.next();
			 
			 int customerid = rs.getInt("customerid");
			 int productid = rs.getInt("productid");
			 toReturn = new Order(orderId,customerid,productid);
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "OrderDa:findById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 return toReturn;
	 }
     
     public static int insert(Order order)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement insertStatement = null;
		 int insertedId= -1;
		 try 
		 {
			 insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			 insertStatement.setInt(1, order.getProductid());
			 insertStatement.setInt(2, order.getCustomerid());
			 insertStatement.executeUpdate();
			 
			 ResultSet rs = insertStatement.getGeneratedKeys();
			 
			 if(rs.next())
			 {
				 insertedId=rs.getInt(1);
			 }
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "OrderDA:insert "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(insertStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 
		 return insertedId;
	 }
     
     public static boolean createOrder(int customerId,int productId,int quantity)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement1 = null;
		 PreparedStatement findStatement2 = null;
		 Order toReturn = null;
		 ResultSet rs = null;
		 ResultSet rs2 = null;
		 boolean ok=true;
		 
		 try 
		 {
			 findStatement1 = dbConnection.prepareStatement(clientStatementString);
			 findStatement1.setLong(1, customerId);
			 rs=findStatement1.executeQuery();
			 rs.next();
			 
			 findStatement2 = dbConnection.prepareStatement(verifyStockStatementString);
			 findStatement2.setLong(1, productId);
			 rs2=findStatement2.executeQuery();
			 rs2.next();
			 
			 String name = rs2.getString("name");
			 int price = rs2.getInt("price");
			 int realQuantity = rs2.getInt("quantity");
			 
			 if(quantity > realQuantity)
			 {
				 JOptionPane.showMessageDialog(null,"Can't complete order : under stock");
				 return false;
			 }
			 
			 
				 Product pnew = new Product(name,price,realQuantity - quantity);
				 ProductDa.update(productId, pnew);
				 
				 Order newOrder = new Order(productId,customerId);
				 OrderDA.insert(newOrder);
				 
				 return true;
			 
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "CustomerDA:updated "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(findStatement1);
			 ConnectionFactory.close(dbConnection);
		 }
		 return true;
		
	 }
	 
}

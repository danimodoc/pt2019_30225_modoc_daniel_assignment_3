package dataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

import connection.ConnectionFactory;
import model.Address;

/**
 *  Class contains the methods that operate on the table from the database
 * 
 */

public class AddressDA {
    
	 protected static final Logger LOGGER = Logger.getLogger(AddressDA.class.getName());
	 private static final String insertStatementString = "INSERT INTO address (zip,city,street,customerid)" + "VALUES(?,?,?,?)";
	 private static final String findStatementString = "SELECT * FROM address where id=?";
	 private static final String deleteStatementString = "delete FROM address where id=?";
	 private static final String editStatementString = "update address set zip=?,city=?,street=?,customerid=? where id=?";
	 
	 
	 public static Address findById(int addressId) {
		 
		 Address toReturn = null;
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(findStatementString);
			 findStatement.setLong(1, addressId);
			 rs=findStatement.executeQuery();
			 rs.next();
			 
			 int zip = rs.getInt("zip");
			 String city = rs.getString("city");
			 String street = rs.getString("street");
			 int customerid = rs.getInt("customerid");
			 toReturn = new Address(zip,city,street,customerid);
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "AddressDa:findById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 return toReturn;
	 }
	 
	 public static int insert(Address Address)
	 {
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement insertStatement = null;
		 int insertedId= -1;
		 try 
		 {
			 insertStatement = dbConnection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			 insertStatement.setInt(1, Address.getZip());
			 insertStatement.setString(2, Address.getCity());
			 insertStatement.setString(3, Address.getStreet());
			 insertStatement.setInt(4, Address.getCustomerid());
			 insertStatement.executeUpdate();
			 
			 ResultSet rs = insertStatement.getGeneratedKeys();
			 
			 if(rs.next())
			 {
				 insertedId=rs.getInt(1);
			 }
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "AddressDA:insert "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(insertStatement);
			 ConnectionFactory.close(dbConnection);
		 }
		 
		 return insertedId;
	 }
	 
//	 public static Address update(int AddressId,Address Address)
//	 {
//		 Connection dbConnection = ConnectionFactory.getConnection();
//		 PreparedStatement editStatement = null;
//		 Address toReturn = null;
//		 
//		 try 
//		 {
//			 editStatement = dbConnection.prepareStatement(editStatementString);
//			 editStatement.setLong(5, AddressId);
//			 editStatement.setString(1, Address.getName());
//			 editStatement.setString(2, Address.getAddress());
//			 editStatement.setString(3, Address.getEmail());
//			 editStatement.setInt(4, Address.getAge());
//			 editStatement.executeUpdate();
//			 
//			 toReturn = new Address(AddressId,Address.getName(),Address.getAddress(),Address.getEmail(),Address.getAge());
//			 
//		 }
//		 catch(SQLException e)
//		 {
//			 LOGGER.log(Level.WARNING, "AddressDA:updated "+e.getMessage());
//		 }
//		 finally
//		 {
//			 ConnectionFactory.close(editStatement);
//			 ConnectionFactory.close(dbConnection);
//		 }
//		 
//		 return toReturn;
//	 }
	 
     public static void deleteById(int AddressId) {
		 
		 Connection dbConnection = ConnectionFactory.getConnection();
		 PreparedStatement findStatement = null;
		 
		 ResultSet rs = null;
		 
		 try
		 {
			 findStatement = dbConnection.prepareStatement(deleteStatementString);
			 findStatement.setLong(1, AddressId);
			 findStatement.executeUpdate();
		 }
		 catch(SQLException e)
		 {
			 LOGGER.log(Level.WARNING, "AddressDa:deleteById "+e.getMessage());
		 }
		 finally
		 {
			 ConnectionFactory.close(rs);
			 ConnectionFactory.close(findStatement);
			 ConnectionFactory.close(dbConnection);
		 }
	 }
	 
	 public static DefaultTableModel buildTableModel(ResultSet rs)
		        throws SQLException {

		    ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

		    // names of columns
		    Vector<String> columnNames = new Vector<String>();
		    int columnCount = metaData.getColumnCount();
		    for (int column = 1; column <= columnCount; column++) {
		        columnNames.add(metaData.getColumnName(column));
		    }

		    // data of the table
		    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		    while (rs.next()) {
		        Vector<Object> vector = new Vector<Object>();
		        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
		            vector.add(rs.getObject(columnIndex));
		        }
		        data.add(vector);
		    }

		    return new DefaultTableModel(data, columnNames);

		}
	 
	
}

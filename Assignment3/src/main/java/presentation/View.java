package presentation;

import javax.swing.*;

/**
 * 
 * @author danimodoc
 * GUI class that shows the main menu and let's you select what you want to do next
 */

public class View extends JFrame {

	JFrame f = new JFrame();
	private JButton clients_btn = new JButton("Manage Clients");
	private JButton products_btn = new JButton("Manage Products");
	private JButton orders_btn = new JButton("Create Order");

	public View() {
		JPanel content1 = new JPanel();
		content1.add(clients_btn);
		// content1.add(products_btn);

		JPanel content2 = new JPanel();
		content2.add(products_btn);
		
		JPanel content3= new JPanel();
		content3.add(orders_btn);

		JPanel content = new JPanel();
		content.add(content1);
		content.add(content2);
		content.add(content3);

		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(600, 300);

		f.setContentPane(content);
		// f.pack();
		f.setTitle("Currency Converter  MVC");
		f.setVisible(true);
		content.setLayout(new BoxLayout(content,BoxLayout.Y_AXIS));
		
		clients_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clients_btnActionPerformed(evt);
            }
        });
		
		products_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product_btnActionPerformed(evt);
            }
        });
		
		orders_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orders_btnActionPerformed(evt);
            }
        });
		
	}
	
	private void clients_btnActionPerformed(java.awt.event.ActionEvent evt) {
		ClientView ob = new ClientView();
		ob.setVisible(true);
		f.setVisible(false);
	}
	
	private void product_btnActionPerformed(java.awt.event.ActionEvent evt) {
		ProductView ob = new ProductView();
		ob.setVisible(true);
		f.setVisible(false);
	}
	
	private void orders_btnActionPerformed(java.awt.event.ActionEvent evt) {
		OrderView ob = new OrderView();
		ob.setVisible(true);
		f.setVisible(false);
	}
	
	
}

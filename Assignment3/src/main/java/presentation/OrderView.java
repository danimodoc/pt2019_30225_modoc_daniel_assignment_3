package presentation;

import java.awt.FlowLayout;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import dataAccessLayer.AddressDA;
import dataAccessLayer.CustomerDa;
import dataAccessLayer.OrderDA;
import model.Address;
import model.Customer;

/**
 * 
 * @author danimodoc
 * 
 * GUI to create a new order
 *
 */

public class OrderView extends JFrame {
	
	private JLabel a1 = new JLabel("Insert Client ID");
	private JLabel a2 = new JLabel("Insert Product ID");
	private JLabel a3 = new JLabel("Insert Quantity");
	private JLabel a4 = new JLabel("Insert Client ZIP");
	private JLabel a5 = new JLabel("Insert Client City");
	private JLabel a6 = new JLabel("Insert Client Street");
	
	private JTextField add1 = new JTextField(10);
	private JTextField add2 = new JTextField(10);
	private JTextField add3 = new JTextField(10);
	private JTextField add4 = new JTextField(10);
	private JTextField add5 = new JTextField(10);
	private JTextField add6 = new JTextField(10);
	
	private JButton back = new JButton("Back");
	private JButton createOrder = new JButton("Create Order");
	private int orderNo=0;
	
	public OrderView()
	{
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();
		JPanel p7 = new JPanel();
		JPanel all = new JPanel();
		
		p1.add(a1);
		p1.add(add1);
		p1.setLayout(new FlowLayout());
		
		p2.add(a2);
		p2.add(add2);
		p2.setLayout(new FlowLayout());
		
		p3.add(a3);
		p3.add(add3);
		p3.setLayout(new FlowLayout());
		
		p5.add(a4);
		p5.add(add4);
		p5.setLayout(new FlowLayout());
		
		p6.add(a5);
		p6.add(add5);
		p6.setLayout(new FlowLayout());
		
		p7.add(a6);
		p7.add(add6);
		p7.setLayout(new FlowLayout());
		
		p4.add(back);
		p4.add(createOrder);
		p4.setLayout(new FlowLayout());
		
		all.add(p1);
		all.add(p2);
		all.add(p3);
		all.add(p5);
		all.add(p6);
		all.add(p7);
		all.add(p4);
		all.setLayout(new BoxLayout(all,BoxLayout.Y_AXIS));
		
		this.setTitle("Order Management");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 900);
		this.setContentPane(all);
		this.pack();
		this.setVisible(true);
		
		back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                back_btnActionPerformed(evt);
            }
        });
		
		createOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createOrder_btnActionPerformed(evt);
            }
        });
		
	}
	
	private void back_btnActionPerformed(java.awt.event.ActionEvent evt) {
		View ob = new View();
		ob.f.setVisible(true);
		this.setVisible(false);
	}
	
	private void createOrder_btnActionPerformed(java.awt.event.ActionEvent evt) {
		boolean ok=OrderDA.createOrder(Integer.parseInt(add1.getText()), Integer.parseInt(add2.getText()), Integer.parseInt(add3.getText()));
		
		if(ok)
		{
		Address c = new Address(Integer.parseInt(add4.getText()),add5.getText(),add6.getText(),Integer.parseInt(add1.getText()));
		AddressDA.insert(c);
		
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("Order"+(++orderNo)+".pdf"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		document.open();
		Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Paragraph chunk = new Paragraph("ORDER"+ ++orderNo, font);
		Paragraph chunk2 = new Paragraph("Client ID :"+add1.getText(), font);
		Paragraph chunk3 = new Paragraph("Product ID :"+add2.getText(), font);
		Paragraph chunk4 = new Paragraph("Quantity :"+add3.getText(), font);
		Paragraph chunk5 = new Paragraph("Client ZIP :"+add4.getText(), font);
		Paragraph chunk6 = new Paragraph("Client City  :"+add5.getText(), font);
		Paragraph chunk7 = new Paragraph("Client Street :"+add6.getText(), font);
		 
		try {
			document.add(chunk);
			document.add(chunk2);
			document.add(chunk3);
			document.add(chunk4);
			document.add(chunk5);
			document.add(chunk6);
			document.add(chunk7);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		
		}
	}
}

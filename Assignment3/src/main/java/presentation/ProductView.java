package presentation;

import java.awt.FlowLayout;

/**
 * GUI to insert,update or delete a product
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import com.mysql.cj.jdbc.result.ResultSetMetaData;
import connection.ConnectionFactory;
import dataAccessLayer.ProductDa;
import model.Product;


public class ProductView extends JFrame {

	JFrame f = new JFrame();
	private JButton add_cl_btn = new JButton("Add New Product");
	private JButton edit_cl_btn = new JButton("Edit Product");
	private JButton find_cl_btn = new JButton("Find Product");
	private JButton del_cl_btn = new JButton("Delete Product");
	private JButton view_cl_btn = new JButton("View Products");
	private JButton back = new JButton("Back");
	
	//private 
	
	private JTextField add1 = new JTextField(10);
	private JTextField add2 = new JTextField(10);
	private JTextField add3 = new JTextField(10);
	private JTextField add4 = new JTextField(10);
	
	private JLabel a1 = new JLabel(" Name");
	private JLabel a2 = new JLabel("Price");
	private JLabel a3 = new JLabel("Quantity");
	private JLabel a4 = new JLabel("Age");
	
	private JTextField edit1 = new JTextField(10);
	private JTextField edit2 = new JTextField(10);
	private JTextField edit3 = new JTextField(10);
	private JTextField edit4 = new JTextField(10);
	private JTextField edit5 = new JTextField(10);
	
	private JLabel e1 = new JLabel("New Name");
	private JLabel e2 = new JLabel("New Price");
	private JLabel e3 = new JLabel("New Quantity");
	private JLabel e5 = new JLabel("                                               Id to find ,edit or delete");
	
	JPanel content = new JPanel();
	JPanel content8 = new JPanel();


	public ProductView() {
		
		JPanel content1 = new JPanel();
		JPanel content2 = new JPanel();
		JPanel content3 = new JPanel();
		JPanel content4 = new JPanel();
		JPanel content5 = new JPanel();
		JPanel content6 = new JPanel();
		JPanel content7 = new JPanel();
		
		
        content1.add(a1);
        content1.add(add1);
        content1.add(e1);
        content1.add(edit1);
        
        content2.add(a2);
        content2.add(add2);
        content2.add(e2);
        content2.add(edit2);
        
        content3.add(a3);
        content3.add(add3);
        content3.add(e3);
        content3.add(edit3);
        
        content5.add(add_cl_btn);
        content5.add(edit_cl_btn);
        content7.add(e5);
        content7.add(edit5);
        content6.add(back);
        
        content5.add(find_cl_btn);
        content5.add(view_cl_btn);
        content5.add(del_cl_btn);
        
        //content8.add(table);
        
        content1.setLayout(new FlowLayout());
        content2.setLayout(new FlowLayout());
        content3.setLayout(new FlowLayout());
        content7.setLayout(new FlowLayout());
        content5.setLayout(new FlowLayout());
        content6.setLayout(new FlowLayout());
        
        content.add(content1);
        content.add(content2);
        content.add(content3);
        content.add(content7);
        content.add(content5);
        content.add(content6); 
        content.add(content8);
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 900);

		this.setTitle("Product Management");
		this.setVisible(true);
		
		content.setLayout(new BoxLayout(content,BoxLayout.Y_AXIS));
		this.setContentPane(content);
		this.pack();
		
		add_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_btnActionPerformed(evt);
            }
        });
		
		find_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                find_btnActionPerformed(evt);
            }
        });
		
		edit_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edit_btnActionPerformed(evt);
            }
        });
		
		del_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_btnActionPerformed(evt);
            }
        });
		
		back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                back_btnActionPerformed(evt);
            }
        });
		
		view_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                view_btnActionPerformed(evt);
            }
        });
		
	}
	
	private void add_btnActionPerformed(java.awt.event.ActionEvent evt) {
		Product c = new Product(add1.getText(),Integer.parseInt(add2.getText()),Integer.parseInt(add3.getText()));
		ProductDa.insert(c);
	}
	
	private void find_btnActionPerformed(java.awt.event.ActionEvent evt) {
		Product c = ProductDa.findById(Integer.parseInt(edit5.getText()));
		edit1.setText(c.getName());
		edit2.setText(String.valueOf(c.getPrice()));
		edit3.setText(String.valueOf(c.getQuantity()));
	}
	
	private void edit_btnActionPerformed(java.awt.event.ActionEvent evt) {
		Product c1 = new Product(edit1.getText(),Integer.parseInt(edit2.getText()),Integer.parseInt(edit3.getText()));
		Product c = ProductDa.update(Integer.parseInt(edit5.getText()),c1);
		edit1.setText(c.getName());
		edit2.setText(String.valueOf(c.getPrice()));
		edit3.setText(String.valueOf(c.getQuantity()));
	}
	
	private void delete_btnActionPerformed(java.awt.event.ActionEvent evt) {
		ProductDa.deleteById(Integer.parseInt(edit5.getText()));
		JOptionPane.showMessageDialog(null,"Product deleted");
	}
	
	private void back_btnActionPerformed(java.awt.event.ActionEvent evt) {
		View ob = new View();
		ob.f.setVisible(true);
		this.setVisible(false);
	}
	
	private void view_btnActionPerformed(java.awt.event.ActionEvent evt) {
		    JTable table = new JTable();
		    Connection dbCon = ConnectionFactory.getConnection();
	        Statement stat;
	        
	        JFrame newn = new JFrame();
	        newn.setSize(400, 300);
	        JPanel all = new JPanel();
	        JPanel p2 = new JPanel();
	        p2.add(new JLabel("Id                Name              Price             Quantity"));
	        JPanel p1 = new JPanel();
	        
			try {
				stat = dbCon.createStatement();
				ResultSet rs = stat.executeQuery("select * from product");
				resultSetToTableModel(rs,table);
				
				p1.add(table);
				all.add(p2);
				all.add(p1);
				all.setLayout(new BoxLayout(all,BoxLayout.Y_AXIS));
				newn.setContentPane(all);
				newn.pack();
				newn.setVisible(true);
				
				//content8.add(table);
				//f.setContentPane(content8);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void resultSetToTableModel(ResultSet rs, JTable table) throws SQLException{
        //Create new table model
        DefaultTableModel tableModel = new DefaultTableModel();

        //Retrieve meta data from ResultSet
        ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

        //Get number of columns from meta data
        int columnCount = metaData.getColumnCount();

        //Get all column names from meta data and add columns to table model
        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++){
            tableModel.addColumn(metaData.getColumnLabel(columnIndex));
        }

        //Create array of Objects with size of column count from meta data
        Object[] row = new Object[columnCount];

        //Scroll through result set
        while (rs.next()){
            //Get object from column with specific index of result set to array of objects
            for (int i = 0; i < columnCount; i++){
                row[i] = rs.getObject(i+1);
            }
            //Now add row to table model with that array of objects as an argument
            tableModel.addRow(row);
        }

        //Now add that table model to your table and you are done :D
        table.setModel(tableModel);
    }

}


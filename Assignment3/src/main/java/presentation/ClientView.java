package presentation;

import java.awt.FlowLayout;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.PreparedStatement;
import com.mysql.cj.jdbc.result.ResultSetMetaData;

import connection.ConnectionFactory;
import dataAccessLayer.CustomerDa;
import model.Customer;

//import net.proteanit.sql.DbUtils;

/**
 * 
 * @author danimodoc
 * GUI to insert , update or delete a customer
 */

public class ClientView extends JFrame {

	JFrame f = new JFrame();
	private JButton add_cl_btn = new JButton("Add New Client");
	private JButton edit_cl_btn = new JButton("Edit Client");
	private JButton find_cl_btn = new JButton("Find Client");
	private JButton del_cl_btn = new JButton("Delete Client");
	private JButton view_cl_btn = new JButton("View Clients");
	private JButton back = new JButton("Back");
	
	//private 
	
	private JTextField add1 = new JTextField(10);
	private JTextField add2 = new JTextField(10);
	private JTextField add3 = new JTextField(10);
	private JTextField add4 = new JTextField(10);
	
	private JLabel a1 = new JLabel("Name");
	private JLabel a2 = new JLabel("Email");
	private JLabel a3 = new JLabel("Address");
	private JLabel a4 = new JLabel("Age");
	
	private JTextField edit1 = new JTextField(10);
	private JTextField edit2 = new JTextField(10);
	private JTextField edit3 = new JTextField(10);
	private JTextField edit4 = new JTextField(10);
	private JTextField edit5 = new JTextField(10);
	
	private JLabel e1 = new JLabel("New Name            ");
	private JLabel e2 = new JLabel("New Email              ");
	private JLabel e3 = new JLabel("New Address       ");
	private JLabel e4 = new JLabel("New Age                  ");
	private JLabel e5 = new JLabel("                                               Id to find ,edit or delete");
	
	JPanel content = new JPanel();
	JPanel content8 = new JPanel();


	public ClientView() {
		
		JPanel content1 = new JPanel();
		JPanel content2 = new JPanel();
		JPanel content3 = new JPanel();
		JPanel content4 = new JPanel();
		JPanel content5 = new JPanel();
		JPanel content6 = new JPanel();
		JPanel content7 = new JPanel();
		
		
        content1.add(a1);
        content1.add(add1);
        content1.add(e1);
        content1.add(edit1);
        
        content2.add(a2);
        content2.add(add2);
        content2.add(e2);
        content2.add(edit2);
        
        content3.add(a3);
        content3.add(add3);
        content3.add(e3);
        content3.add(edit3);
        
        content4.add(a4);
        content4.add(add4);
        content4.add(e4);
        content4.add(edit4);
        
        content5.add(add_cl_btn);
        content5.add(edit_cl_btn);
        content7.add(e5);
        content7.add(edit5);
        content6.add(back);
        
        content5.add(find_cl_btn);
        content5.add(view_cl_btn);
        content5.add(del_cl_btn);
        
        //content8.add(table);
        
        content1.setLayout(new FlowLayout());
        content2.setLayout(new FlowLayout());
        content3.setLayout(new FlowLayout());
        content4.setLayout(new FlowLayout());
        content7.setLayout(new FlowLayout());
        content5.setLayout(new FlowLayout());
        content6.setLayout(new FlowLayout());
        
        content.add(content1);
        content.add(content2);
        content.add(content3);
        content.add(content4);
        content.add(content7);
        content.add(content5);
        content.add(content6); 
        content.add(content8);
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 900);

		this.setTitle("Customers Management");
		this.setVisible(true);
		
		content.setLayout(new BoxLayout(content,BoxLayout.Y_AXIS));
		this.setContentPane(content);
		this.pack();
		
		add_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add_btnActionPerformed(evt);
            }
        });
		
		find_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                find_btnActionPerformed(evt);
            }
        });
		
		edit_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edit_btnActionPerformed(evt);
            }
        });
		
		del_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delete_btnActionPerformed(evt);
            }
        });
		
		back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                back_btnActionPerformed(evt);
            }
        });
		
		view_cl_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                view_btnActionPerformed(evt);
            }
        });
		
	}
	
	private void add_btnActionPerformed(java.awt.event.ActionEvent evt) {
		Customer c = new Customer(add1.getText(),add2.getText(),add3.getText(),Integer.parseInt(add4.getText()));
		CustomerDa.insert(c);
	}
	
	private void find_btnActionPerformed(java.awt.event.ActionEvent evt) {
		Customer c = CustomerDa.findById(Integer.parseInt(edit5.getText()));
		edit1.setText(c.getName());
		edit2.setText(c.getAddress());
		edit3.setText(c.getEmail());
		edit4.setText(String.valueOf(c.getAge()));
	}
	
	private void edit_btnActionPerformed(java.awt.event.ActionEvent evt) {
		Customer c1 = new Customer(edit1.getText(),edit2.getText(),edit3.getText(),Integer.parseInt(edit4.getText()));
		Customer c = CustomerDa.update(Integer.parseInt(edit5.getText()),c1);
		edit1.setText(c.getName());
		edit2.setText(c.getAddress());
		edit3.setText(c.getEmail());
		edit4.setText(String.valueOf(c.getAge()));
	}
	
	private void delete_btnActionPerformed(java.awt.event.ActionEvent evt) {
		CustomerDa.deleteById(Integer.parseInt(edit5.getText()));
		JOptionPane.showMessageDialog(null,"Utilizator eliminat");
	}
	
	private void back_btnActionPerformed(java.awt.event.ActionEvent evt) {
		View ob = new View();
		ob.f.setVisible(true);
		this.setVisible(false);
	}
	
	private void view_btnActionPerformed(java.awt.event.ActionEvent evt) {
		    JTable table = new JTable();
		    Connection dbCon = ConnectionFactory.getConnection();
	        Statement stat;
	        
//	        JFrame newn = new JFrame();
//	        newn.setSize(600, 500);
//	        JPanel p1 = new JPanel();
	        
	        JFrame newn = new JFrame();
	        newn.setSize(400, 300);
	        JPanel all = new JPanel();
	        JPanel p2 = new JPanel();
	        p2.add(new JLabel("Id                Name              Email            Address              Age"));
	        JPanel p1 = new JPanel();
	        
			try {
				stat = dbCon.createStatement();
				ResultSet rs = stat.executeQuery("select * from customer");
				resultSetToTableModel(rs,table);
				
//				p1.add(table);
//				newn.setContentPane(p1);
//				newn.setVisible(true);
				p1.add(table);
				all.add(p2);
				all.add(p1);
				all.setLayout(new BoxLayout(all,BoxLayout.Y_AXIS));
				newn.setContentPane(all);
				newn.pack();
				newn.setVisible(true);
				
				//content8.add(table);
				//f.setContentPane(content8);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void resultSetToTableModel(ResultSet rs, JTable table) throws SQLException{
        //Create new table model
        DefaultTableModel tableModel = new DefaultTableModel();

        //Retrieve meta data from ResultSet
        ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();

        //Get number of columns from meta data
        int columnCount = metaData.getColumnCount();

        //Get all column names from meta data and add columns to table model
        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++){
            tableModel.addColumn(metaData.getColumnLabel(columnIndex));
        }

        //Create array of Objects with size of column count from meta data
        Object[] row = new Object[columnCount];

        //Scroll through result set
        while (rs.next()){
            //Get object from column with specific index of result set to array of objects
            for (int i = 0; i < columnCount; i++){
                row[i] = rs.getObject(i+1);
            }
            //Now add row to table model with that array of objects as an argument
            tableModel.addRow(row);
        }

        //Now add that table model to your table and you are done :D
        table.setModel(tableModel);
    }

}
